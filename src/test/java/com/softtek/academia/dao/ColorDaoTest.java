package com.softtek.academia.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.softtek.academia.model.Color;

public class ColorDaoTest {

	private ColorDAO colorDao;

	@BeforeEach
	public void initJDBC() {
		this.colorDao = new ColorJDBCImpl();
	}
	
	@Test
	public void testGetAllColors() {
		//setup
		Color color = new Color(1, "White", "#FFFFFF");
		//execute
		List<Color> colors = this.colorDao.getAll(); 
		
		//verify
		assertNotNull(colors, "Color's list is empty");
		Color actualColor = colors.get(0);
		assertEquals(color.getName(), actualColor.getName(),
				"Colors don't match");
	}
	
	@Test
	public void testGetColorById() {
		//setup
		long id = 2;
		Color expectedColor = new Color(2, "Silver", "#C0C0C0");
		//execute
		Color actualColor = colorDao.getById(id);
		//validate
		assertNotNull(actualColor, "ColorById is empty");
		assertEquals(expectedColor.getName(), actualColor.getName(),
				"Colors don't match");
	}
}
