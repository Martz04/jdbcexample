package com.softtek.academia.service;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.softtek.academia.dao.ColorDAO;
import com.softtek.academia.dao.ColorJDBCImpl;
import com.softtek.academia.model.Color;

public class ColorFilterTest {

	@Test
	public void testNoBlackColor() {
		//setup
		ColorDAO colorDao = new ColorJDBCImpl();
		ColorService colorService = new ColorServiceImpl(colorDao);
		
		//execute
		List<Color> filteredList = colorService.getFilterList();
		
		//validate
		assertNotNull(filteredList, "Filtered list is empty");
		filteredList.forEach((c) -> {
			assertNotEquals("Black", c.getName());
		});
	}
}
