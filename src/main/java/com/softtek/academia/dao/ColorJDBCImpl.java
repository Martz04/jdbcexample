package com.softtek.academia.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academia.model.Color;

public class ColorJDBCImpl implements ColorDAO {

	private static Connection CONECTION = null;
	
	private static Connection getConnection() {
		if(CONECTION == null) {
			try {
				CONECTION = DriverManager.getConnection(
			            "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234");
			} catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			}
		}
		return CONECTION;
	}
	
	public static void closeConnection() {
		try {
			getConnection().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private PreparedStatement getReadyForQuery(String query) throws SQLException {
		
		PreparedStatement preparedStatement = null;   
        preparedStatement = ColorJDBCImpl.getConnection().prepareStatement(query);
		
		return preparedStatement;
	}
	
	@Override
	public List<Color> getAll() {
		List<Color> result = new ArrayList<>();

        String SQL_SELECT = "Select * from COLOR";
        
    	// auto close connection
        try {

                PreparedStatement preparedStatement = getReadyForQuery(SQL_SELECT);
                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    long id = resultSet.getLong("COLORID");
                    String name = resultSet.getString("NAME");
                    String hexValue = resultSet.getString("HEXVALUE");
                    
                    // --
                    Color color = new Color(id, name, hexValue);
                    result.add(color);
                    
                }
           
        } catch (Exception e) {
            e.printStackTrace();
        } 
        // --
        return result;
	}

	@Override
	public Color getById(long id) {
		String SQL_SELECT = "Select * from COLOR where colorId = ?";
		Color color = null;
    	// auto close connection
        try {

                PreparedStatement preparedStatement = getReadyForQuery(SQL_SELECT);
                preparedStatement.setLong(1, id);
                
                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    long colorId = resultSet.getLong("COLORID");
                    String name = resultSet.getString("NAME");
                    String hexValue = resultSet.getString("HEXVALUE");
                    
                    // --
                    color = new Color(colorId, name, hexValue);
                    
                }
           
        } catch (Exception e) {
            e.printStackTrace();
        } 
		return color;
	}

}
