package com.softtek.academia.dao;

import java.util.List;

import com.softtek.academia.model.Color;

public interface ColorDAO {

	List<Color> getAll();

	Color getById(long id);
}
