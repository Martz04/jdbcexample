package com.softtek.academia.JdbcExample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academia.dao.ColorJDBCImpl;
import com.softtek.academia.model.Color;
import com.softtek.academia.service.ColorServiceImpl;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	System.out.println("MySQL JDBC Connection");
    	ColorJDBCImpl colorDao = new ColorJDBCImpl();
    	ColorServiceImpl colorService 
    		= new ColorServiceImpl(colorDao);
    	
    	List<Color> colors = colorService.getAll();
    	colors.forEach(System.out::println);
    }
}
