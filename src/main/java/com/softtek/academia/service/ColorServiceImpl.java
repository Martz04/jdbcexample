package com.softtek.academia.service;

import java.util.List;
import java.util.stream.Collectors;

import com.softtek.academia.dao.ColorDAO;
import com.softtek.academia.model.Color;

public class ColorServiceImpl implements ColorService {

	private ColorDAO colorDao;
	
	public ColorServiceImpl(ColorDAO colorDao) {
		this.colorDao = colorDao;
	}
	
	@Override
	public Color getById(long id) {
		// TODO Auto-generated method stub
		return colorDao.getById(id);
	}


	@Override
	public List<Color> getAll() {
		return colorDao.getAll();
	}

	@Override
	public List<Color> getFilterList() {
		List<Color> colors = colorDao.getAll();
		colors.removeIf((c) -> c.getName().equals("Black"));
		
		return colors;
	}

}
