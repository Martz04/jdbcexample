package com.softtek.academia.service;

import java.util.List;

import com.softtek.academia.model.Color;

public interface ColorService {

	List<Color> getAll();
	
	Color getById(long id);

	List<Color> getFilterList();
}
