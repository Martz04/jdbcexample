package com.softtek.academia.model;

public class Color {

	private long colorId;
	private String name;
	private String hexValue;
	
	public Color() { }
	
	public Color(long colorId, String name, String hexValue) {
		super();
		this.colorId = colorId;
		this.name = name;
		this.hexValue = hexValue;
	}
	
	public long getColorId() {
		return colorId;
	}
	public void setColorId(long colorId) {
		this.colorId = colorId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHexValue() {
		return hexValue;
	}
	public void setHexValue(String hexValue) {
		this.hexValue = hexValue;
	}

	@Override
	public String toString() {
		return "Color [colorId=" + colorId + ", name=" + name + ", hexValue=" + hexValue + "]";
	}
	
}
